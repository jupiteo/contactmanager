﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using ContactManager.Models;

namespace ContactManager
{
    public class DatabaseInitializer
    {
        public static void Initialize()
        {
            using (var context = new ContactsContext())
            {                
                context.Database.Initialize(true);
            }

            WebSecurityInitializer.Instance.EnsureInitialized();
        }
    }
}