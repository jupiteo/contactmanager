﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Security;
using ContactManager.Migrations;
using WebMatrix.WebData;

namespace ContactManager.Models
{
    public class ContactsContext : DbContext
    {
        public ContactsContext()
            : base("DefaultConnection")
        {
        }

        public DbSet<User> Users { get; set; }
        public DbSet<Contact> Contacts { get; set; }
        
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {   
            Database.SetInitializer(new MigrateDatabaseToLatestVersion<ContactsContext, Configuration>());
        }

        internal void Seed()
        {
            SeedMembership();
        }

        internal void SeedMembership()
        {
            WebSecurityInitializer.Instance.EnsureInitialized();

            var roles = (SimpleRoleProvider)Roles.Provider;
            var membership = (SimpleMembershipProvider)Membership.Provider;

            if (!roles.RoleExists("Admin"))
            {
                roles.CreateRole("Admin");
            }

            if (membership.GetUser("pascal", false) == null)
            {
                membership.CreateUserAndAccount("pascal", "pascal");

                if (!roles.IsUserInRole("pascal", "Admin"))
                {
                    roles.AddUsersToRoles(new string[] { "pascal" }, new string[] { "Admin" });
                }
            }
        }
    }
}