﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WebMatrix.WebData;

namespace ContactManager.Models
{
    public class WebSecurityInitializer
    {
        private bool isInitialized = false;
        private object syncLock = new object();

        public static readonly WebSecurityInitializer Instance = new WebSecurityInitializer();

        private WebSecurityInitializer() { }

        public void EnsureInitialized()
        {
            if (!isInitialized)
            {
                lock (syncLock)
                {
                    if (!isInitialized)
                    {
                        isInitialized = true;
                        WebSecurity.InitializeDatabaseConnection("DefaultConnection",
                            "Users", "UserId", "UserName", true);
                    }
                }
            }
        }
    }
}