﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ContactManager.Models;
using ContactManager.Repositories;

namespace ContactManager.Controllers
{
    public class ContactController : Controller
    {
        private IContactRepository contactRepository;

        public ContactController(): this(new ContactRepository())
        {
        }

        public ContactController(IContactRepository repository)
        {
            contactRepository = repository;
        }

        //
        // GET: /Contact/

        public ActionResult Index()
        {
            return View(contactRepository.GetAll());
        }

        //
        // GET: /Contact/Details/5

        public ActionResult Details(int id = 0)
        {
            Contact contact = contactRepository.GetById(id);
            if (contact == null)
            {
                return HttpNotFound();
            }
            return View(contact);
        }

        //
        // GET: /Contact/Create

        public ActionResult Create()
        {
            return View();
        }

        //
        // POST: /Contact/Create

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(Contact contact)
        {
            if (ModelState.IsValid)
            {
                contactRepository.Add(contact);
                contactRepository.SaveChanges();
                
                return RedirectToAction("Index");
            }

            return View(contact);
        }

        //
        // GET: /Contact/Edit/5

        public ActionResult Edit(int id = 0)
        {
            Contact contact = contactRepository.GetById(id);
            if (contact == null)
            {
                return HttpNotFound();
            }
            return View(contact);
        }

        //
        // POST: /Contact/Edit/5

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(int id, FormCollection form)
        {
            Contact contact = contactRepository.GetById(id);
            if (ModelState.IsValid)
            {
                UpdateModel(contact, form.ToValueProvider());
                contactRepository.SaveChanges();
                
                return RedirectToAction("Index");
            }
            return View(contact);
        }

        //
        // GET: /Contact/Delete/5

        public ActionResult Delete(int id = 0)
        {
            Contact contact = contactRepository.GetById(id);
            if (contact == null)
            {
                return HttpNotFound();
            }
            return View(contact);
        }

        //
        // POST: /Contact/Delete/5

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Contact contact = contactRepository.GetById(id);
            
            contactRepository.Remove(contact);
            contactRepository.SaveChanges();

            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            contactRepository.Dispose();
            base.Dispose(disposing);
        }
    }
}