﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ContactManager.Models;

namespace ContactManager.Repositories
{
    public class ContactRepository: IDisposable, IContactRepository
    {
        private ContactsContext db = new ContactsContext();

        public IList<Contact> GetAll()
        {
            return db.Contacts.ToList();
        }

        public Contact GetById(int id)
        {
            return db.Contacts.Find(id);
        }

        public void Add(Contact contact)
        {
            db.Contacts.Add(contact);
        }

        public void SaveChanges()
        {
            db.SaveChanges();
        }

        public void Remove(Contact contact)
        {
            db.Contacts.Remove(contact);
        }

        public void Dispose()
        {
            db.Dispose();
        }
    }
}