﻿using System;
using System.Collections.Generic;
using ContactManager.Models;
namespace ContactManager.Repositories
{
    public interface IContactRepository: IDisposable
    {
        void Add(Contact contact);

        IList<Contact> GetAll();

        Contact GetById(int id);

        void Remove(Contact contact);

        void SaveChanges();
    }
}
