﻿Application de démo pour le live coding sur dotnetdojo.com

Objectif :
Créer une application permettant de gérer ses contacts (téléphoniques avec email éventuellement).

User stories :
--------------

S1 : En tant qu'utilisateur, je souhaite pouvoir afficher la liste de mes contacts.

S2 : En tant qu'utilisateur, je souhaite pouvoir afficher le détail d'un contact.

S3 : En tant qu'utilisateur, je souhaite pouvoir modifier les informations d'un contact.

S4 : En tant qu'utilisateur, je souhaite pouvoir ajouter un nouveau contact.

S5 : En tant qu'utilisateur, je souhaite pouvoir supprimer un contact existant.

S6 : En tant qu'utilisateur, je souhaite pouvoir exporter ma liste de contact (afin d'en faire une sauvegarde).

Technos :
---------

- ASP.NET MVC 4
- Base de données SQL Server (LocalDB pour le développement)
- Entity Framework Code First
- Archi simple pour démarrer (basé sur IoC)
- Tests unitaires (TDD)
- Hébergement sur Windows Azure
