﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using ContactManager.Controllers;
using ContactManager.Models;
using ContactManager.Repositories;
using FakeItEasy;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace ContactManager.Tests.Controllers
{
    [TestClass]
    public class ContactControllerTest
    {
        [TestMethod]
        public void Index_BddVide_ListeVide()
        {
            // Arrange
            var fakeRepository = A.Fake<IContactRepository>();
            var listeContactsVide = new List<Contact>();

            A.CallTo(() => fakeRepository.GetAll()).Returns(listeContactsVide);

            ContactController controller = new ContactController(fakeRepository);

            // Act
            ViewResult result = controller.Index() as ViewResult;

            // Assert
            Assert.IsNotNull(result);   // j'ai une vue
            Assert.IsTrue(result.Model is IList<Contact>);
        }

        [TestMethod]
        [ExpectedException(typeof(ApplicationException))]
        public void Index_BddException_RetourneException()
        {
            // Arrange
            var fakeRepository = A.Fake<IContactRepository>();

            A.CallTo(() => fakeRepository.GetAll()).Throws(new ApplicationException());

            ContactController controller = new ContactController(fakeRepository);

            // Act
            ViewResult result = controller.Index() as ViewResult;
        }

        [TestMethod]
        public void Details_ContactId1_RetourneContact()
        {
            // Arrange
            var fakeRepository = A.Fake<IContactRepository>();
            var aContact = new Contact();

            A.CallTo(() => fakeRepository.GetById(1)).Returns(aContact);

            ContactController controller = new ContactController(fakeRepository);

            // Act
            ViewResult result = controller.Details(1) as ViewResult;
            Assert.IsNotNull(result);
            Assert.AreEqual(aContact, result.Model);
        }

        [TestMethod]
        public void Details_ContactIdNonExistant_RetourneContact()
        {
            // Arrange
            var fakeRepository = A.Fake<IContactRepository>();            

            A.CallTo(() => fakeRepository.GetById(10)).Returns(null);

            ContactController controller = new ContactController(fakeRepository);

            // Act
            var result = controller.Details(10) as HttpNotFoundResult;
            Assert.IsNotNull(result);
        }

        [TestMethod]
        public void Create_CreerNormal_CreationBdd()
        {
            // Arrange
            var fakeRepository = A.Fake<IContactRepository>();
            
            ContactController controller = new ContactController(fakeRepository);

            var aContact = new Contact() { FirstName = "First", LastName = "Last", PhoneNumber = "123" };

            // Act
            var result = controller.Create(aContact) as RedirectToRouteResult;
            Assert.IsNotNull(result);

            A.CallTo(() => fakeRepository.Add(A<Contact>.Ignored)).MustHaveHappened();
            A.CallTo(() => fakeRepository.SaveChanges()).MustHaveHappened();
        }

        // TODO: écrire un test avec ModelState.IsValid = false (dès que règles en place)

        [TestMethod]
        public void Edit_ChargerExistant_AfficherFormulaire()
        {
            // Arrange
            var fakeRepository = A.Fake<IContactRepository>();
            var aContact = new Contact() { ContactId = 2, FirstName = "First", LastName = "Last", PhoneNumber = "888" };

            A.CallTo(() => fakeRepository.GetById(2)).Returns(aContact);            

            ContactController controller = new ContactController(fakeRepository);            

            // Act
            var result = controller.Edit(2) as ViewResult;
            Assert.IsNotNull(result);
            Assert.AreEqual(aContact, result.Model);
        }

        [TestMethod]
        public void Edit_ChargerNonExistant_AfficherErreur()
        {
            // Arrange
            var fakeRepository = A.Fake<IContactRepository>();            

            A.CallTo(() => fakeRepository.GetById(1)).Returns(null);

            ContactController controller = new ContactController(fakeRepository);

            // Act
            var result = controller.Edit(1) as HttpNotFoundResult;
            Assert.IsNotNull(result);            
        }

        [TestMethod]
        public void Edit_SauvegarderExistant_MetAJourExistant()
        {
            // Arrange
            var fakeRepository = A.Fake<IContactRepository>();
            var dbContact = new Contact() { ContactId = 1, FirstName = "First", LastName = "Last", PhoneNumber = "888" };

            A.CallTo(() => fakeRepository.GetById(1)).Returns(dbContact);

            // Mise en place d'un contexte special pour ce controlleur pour injecter une FormCollection
            var routeData = new RouteData();
            var httpContext = A.Fake<HttpContextBase>();

            ContactController controller = new ContactController(fakeRepository);

            var fakeControllerContext = new ControllerContext(httpContext, routeData, controller);
            // ControllerContext est nécessaire pour UpdateModel()
            controller.ControllerContext = fakeControllerContext;

            // Act
            var formContact = new Contact() { ContactId = 1, FirstName = "NewFirst", LastName = "NewLast", PhoneNumber = "555888" };

            FormCollection formParameters = new FormCollection();
            formParameters.Add("FirstName", formContact.FirstName);
            formParameters.Add("LastName", formContact.LastName);
            formParameters.Add("PhoneNumber", formContact.PhoneNumber);
            formParameters.Add("ContactId", formContact.ContactId.ToString());

            var result = controller.Edit(1, formParameters) as RedirectToRouteResult;
            Assert.IsNotNull(result);
            A.CallTo(() => fakeRepository.SaveChanges()).MustHaveHappened();

            // Check updates
            Assert.AreEqual(dbContact.FirstName, formContact.FirstName);
            Assert.AreEqual(dbContact.LastName, formContact.LastName);
            Assert.AreEqual(dbContact.PhoneNumber, formContact.PhoneNumber);
        }
    }
}
